%% Copyright (c) 2022 The Tor Project, inc. All rights reserved.
%% Use of this source code is governed by a BSD-style
%% license that can be found in the LICENSE file.

\begin{frame}[plain,noframenumbering]
    \titlepage
\end{frame}

\begin{frame}
    This presentation explains some of the \highlight{ongoing and upcoming
    efforts within The Tor Project to modernize our ecosystem.}

    We will be \highlight{focusing on the Tor Network itself} and the
    components used to run the network.

    We will not be covering efforts happening around Anti-censorship
    technology, Tor Browser, Localization, Metrics, UI, UX, and the other
    exciting things happening inside the Tor Project right now.
\end{frame}

\begin{frame}
    \frametitle{About Me}

    \begin{columns}
        \begin{column}{0.65\textwidth}
            \begin{itemize}
                \item Core Developer at The Tor Project since early 2017.
                      Team Lead of the Network Team since late 2019.
                \item Free Software developer since 2006.
                \item Co-organizing the annual Danish hacker festival
                      \href{https://bornhack.dk/}{BornHack} on Funen.
                \item First hackercamp, out of many, was OHM in 2013.
            \end{itemize}
        \end{column}

        \begin{column}{0.35\textwidth}
            \begin{center}
                \includegraphics[width=0.95\textwidth]{images/tor_man.png}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{What is Tor?}

    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Online anonymity, and censorship circumvention.
                    \begin{itemize}
                        \item Free software.
                        \item Open network.
                    \end{itemize}
                \item Community of researchers, developers, users, and relay operators.
                \item U.S. 501(c)(3) non-profit organization.
            \end{itemize}
        \end{column}

        \begin{column}{0.4\textwidth}
            \begin{center}
                \includegraphics[width=0.95\textwidth]{images/what_is_tor.jpg}
            \end{center}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[plain]
    \tikzset{external/export next=false}

    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.north), yshift=-2.5cm, font=\bfseries] {Somewhere between 2,000,000 and 8,000,000 daily users.};
        \node[at=(current page.center), yshift=-2.5cm, align=center] {\input{images/tor_group.tex}};
    \end{tikzpicture}
\end{frame}

\begin{frame}[t]
    \frametitle{How does Tor work?}

    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        %% The Anonymity Network cloud.
        \node[] at (0, 2) {The Tor Network};
        \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1] at (0,0) {};

        %% The relay nodes inside the Anonymity Network cloud.
        \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
        \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
        \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
        \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
        \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
        \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
        \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}

    Alice picks a path through the network: \(R_{1}\), \(R_{2}\), and
    \(R_{3}\) before finally reaching Bob.
\end{frame}

\begin{frame}[t]
    \frametitle{How does Tor work?}

    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

        \pgfdeclarelayer{background}
        \pgfdeclarelayer{foreground}
        \pgfsetlayers{background,main,foreground}

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        %% The Anonymity Network cloud.
        \begin{pgfonlayer}{background}
            \node[] at (0, 2) {The Tor Network};
            \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
        \end{pgfonlayer}

        %% The relay nodes inside the Anonymity Network cloud.
        \begin{pgfonlayer}{foreground}
            \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
            \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
            \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
            \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
            \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
            \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
            \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
        \end{pgfonlayer}

        %% Alice connects to R1.
        \path[line width=4.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}

    Alice makes a session key with \(R_{1}\).
\end{frame}

\begin{frame}[t]
    \frametitle{How does Tor work?}

    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

        \pgfdeclarelayer{background}
        \pgfdeclarelayer{foreground}
        \pgfsetlayers{background,main,foreground}

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        %% The Anonymity Network cloud.
        \begin{pgfonlayer}{background}
            \node[] at (0, 2) {The Tor Network};
            \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
        \end{pgfonlayer}

        %% The relay nodes inside the Anonymity Network cloud.
        \begin{pgfonlayer}{foreground}
            \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
            \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
            \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
            \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
            \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
            \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
            \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
        \end{pgfonlayer}

        %% Alice connects to R1.
        \path[line width=8.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
        \path[line width=4.0pt,draw=blue!80] (-4.2, -0.4) edge (r1.center);

        %% R1 connects to R2.
        \path[line width=4.0pt,draw=blue!80] (r1.center) edge (r2.center);

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}

    Alice asks \(R_{1}\) to extend to \(R_{2}\).
\end{frame}

\begin{frame}[t]
    \frametitle{How does Tor work?}

    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

        \pgfdeclarelayer{background}
        \pgfdeclarelayer{foreground}
        \pgfsetlayers{background,main,foreground}

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        %% The Anonymity Network cloud.
        \begin{pgfonlayer}{background}
            \node[] at (0, 2) {The Tor Network};
            \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
        \end{pgfonlayer}

        %% The relay nodes inside the Anonymity Network cloud.
        \begin{pgfonlayer}{foreground}
            \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
            \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
            \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
            \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
            \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
            \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
            \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
        \end{pgfonlayer}

        %% Alice connects to R1.
        \path[line width=12.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
        \path[line width=8.0pt,draw=blue!80] (-4.2, -0.4) edge (r1.center);
        \path[line width=4.0pt,draw=yellow!80] (-4.2, -0.4) edge (r1.center);

        %% R1 connects to R2.
        \path[line width=8.0pt,draw=blue!80]   (r1.center) edge (r2.center);
        \path[line width=4.0pt,draw=yellow!80] (r1.center) edge (r2.center);

        %% R2 connects to R3.
        \path[line width=4.0pt,draw=yellow!80] (r2.center) edge (r3.center);

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}

    Alice asks \(R_{2}\) to extend to \(R_{3}\).
\end{frame}

\begin{frame}[t]
    \frametitle{How does Tor work?}

    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

        \pgfdeclarelayer{background}
        \pgfdeclarelayer{foreground}
        \pgfsetlayers{background,main,foreground}

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        %% The Anonymity Network cloud.
        \begin{pgfonlayer}{background}
            \node[] at (0, 2) {The Tor Network};
            \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
        \end{pgfonlayer}

        %% The relay nodes inside the Anonymity Network cloud.
        \begin{pgfonlayer}{foreground}
            \node[relay,ultra thick,draw=green!80]  (r1) at (-1.9, 0.2)  {$R_{1}$};
            \node[relay,ultra thick,draw=blue!80]   (r2) at (0.0, 0.1)   {$R_{2}$};
            \node[relay,ultra thick,draw=yellow!80] (r3) at (1.8, -0.4)  {$R_{3}$};
            \node[relay] (r4) at (-0.7, 0.7)  {$R_{4}$};
            \node[relay] (r5) at (-1.0, -0.7) {$R_{5}$};
            \node[relay] (r6) at (0.8, -0.6)  {$R_{6}$};
            \node[relay] (r7) at (1.2, 0.5)   {$R_{7}$};
        \end{pgfonlayer}

        %% Alice connects to R1.
        \path[line width=12.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
        \path[line width=8.0pt,draw=blue!80]   (-4.2, -0.4) edge (r1.center);
        \path[line width=4.0pt,draw=yellow!80] (-4.2, -0.4) edge (r1.center);
        \path[thick,draw=black           ]     (-4.2, -0.4) edge (r1.center);

        %% R1 connects to R2.
        \path[line width=8.0pt,draw=blue!80]   (r1.center) edge (r2.center);
        \path[line width=4.0pt,draw=yellow!80] (r1.center) edge (r2.center);
        \path[thick,draw=black]                (r1.center) edge (r2.center);

        %% R2 connects to R3.
        \path[line width=4.0pt,draw=yellow!80] (r2.center) edge (r3.center);
        \path[thick,draw=black]                (r2.center) edge (r3.center);

        %% R3 connects to Bob.
        \path[thick,draw=black] (r3.center) edge (4.4, -0.4);

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}

    Alice finally asks \(R_{3}\) to connect to Bob.
\end{frame}

\begin{frame}[t]
    \frametitle{How does Tor work?}

    \centering
    \begin{tikzpicture}
        %% Define the style for our relay nodes inside the Anonymity Network cloud.
        \tikzstyle{relay}=[circle, draw, thin, fill=OnionDarkPurple!80, text=white, font=\scriptsize, scale=0.8]

        \pgfdeclarelayer{background}
        \pgfdeclarelayer{foreground}
        \pgfsetlayers{background,main,foreground}

        %% Alice.
        \node[] at (-6, 2.5) {Alice};
        \node[alice, monitor, minimum size=1.6cm] (alice) at (-6, 0.5) {};

        %% Bob.
        \node[] at (6, 2.5) {Bob};
        \node[bob, mirrored, monitor, minimum size=1.6cm] (bob) at (6, 0.5) {};

        %% The Anonymity Network cloud.
        \begin{pgfonlayer}{background}
            \node[] at (0, 2) {The Tor Network};
            \node[cloud, fill=OnionPurple!40, cloud puffs=16, cloud puff arc=100, minimum width=5.5cm, minimum height=2.6cm, aspect=1, on background layer] at (0,0) {};
        \end{pgfonlayer}

        %% The relay nodes inside the Anonymity Network cloud.
        \begin{pgfonlayer}{foreground}
            \node[relay,ultra thick,draw=green!80, minimum size=1.3cm]  (r1) at (-1.9, 0.2)  {Guard};
            \node[relay,ultra thick,draw=blue!80, minimum size=1.3cm]   (r2) at (0.0, 0.1)   {Middle};
            \node[relay,ultra thick,draw=yellow!80, minimum size=1.3cm] (r3) at (1.8, -0.4)  {Exit};
        \end{pgfonlayer}

        %% Alice connects to R1.
        \path[line width=12.0pt,draw=green!80] (-4.2, -0.4) edge (r1.center);
        \path[line width=8.0pt,draw=blue!80]   (-4.2, -0.4) edge (r1.center);
        \path[line width=4.0pt,draw=yellow!80] (-4.2, -0.4) edge (r1.center);
        \path[thick,draw=black           ]     (-4.2, -0.4) edge (r1.center);

        %% R1 connects to R2.
        \path[line width=8.0pt,draw=blue!80]   (r1.center) edge (r2.center);
        \path[line width=4.0pt,draw=yellow!80] (r1.center) edge (r2.center);
        \path[thick,draw=black]                (r1.center) edge (r2.center);

        %% R2 connects to R3.
        \path[line width=4.0pt,draw=yellow!80] (r2.center) edge (r3.center);
        \path[thick,draw=black]                (r2.center) edge (r3.center);

        %% R3 connects to Bob.
        \path[thick,draw=black] (r3.center) edge (4.4, -0.4);

        %% Helper lines for debugging.
        %% \draw[help lines] (-7,-3) grid (7,3);
    \end{tikzpicture}
\end{frame}

\begin{frame}[plain]{}
    \tikzset{external/export next=false}
    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.center)] {Speed $>$ Blocking $>$ Privacy $>$ Security $>$ UI};
    \end{tikzpicture}
\end{frame}

\begin{frame}[plain]{}
    \tikzset{external/export next=false}
    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.center)] {\textcolor{lime}{Speed} $>$ Blocking $>$ Privacy $>$ Security $>$ UI};
        \node[text=white, at=(current page.south), yshift=2cm] {\textcolor{white}{The performance of the Tor network: its throughput and latency.}};
    \end{tikzpicture}
\end{frame}

\begin{frame}[plain]{}
    \tikzset{external/export next=false}
    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.center)] {Speed $>$ \textcolor{lime}{Blocking} $>$ Privacy $>$ Security $>$ UI};
        \node[text=white, at=(current page.south), yshift=2cm] {\textcolor{white}{Website blocking, captcha portals, denial of access, etc.}};
    \end{tikzpicture}
\end{frame}

\begin{frame}[plain]{}
    \tikzset{external/export next=false}
    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.center)] {Speed $>$ Blocking $>$ \textcolor{lime}{Privacy} $>$ Security $>$ UI};
        \node[text=white, at=(current page.south), yshift=2cm] {\textcolor{white}{Privacy and anonymity guarantees by Tor.}};
    \end{tikzpicture}
\end{frame}

\begin{frame}[plain]{}
    \tikzset{external/export next=false}
    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.center)] {Speed $>$ Blocking $>$ Privacy $>$ \textcolor{lime}{Security} $>$ UI};
        \node[text=white, at=(current page.south), yshift=2cm] {\textcolor{white}{Security of the different Tor components.}};
    \end{tikzpicture}
\end{frame}

\begin{frame}[plain]{}
    \tikzset{external/export next=false}
    \begin{tikzpicture}[remember picture, overlay, background rectangle/.style={fill=OnionDarkPurple}, show background rectangle]
        \node[text=white, at=(current page.center)] {Speed $>$ Blocking $>$ Privacy $>$ Security $>$ \textcolor{lime}{UI}};
        \node[text=white, at=(current page.south), yshift=2cm] {\textcolor{white}{User interfaces of our products.}};
    \end{tikzpicture}
\end{frame}
