TARGET     = main.pdf
SOURCE     = main.tex
SOURCES    = $(wildcard *.tex)

DATA_START_DATE ?= 2010-01-01
DATA_END_DATE   ?= 2022-08-01

export TEXINPUTS:=$(shell pwd)/onion-tex/src/tex:${TEXINPUTS}

all: $(TARGET)

$(TARGET): tikz $(SOURCES) images/*
	latexmk -g -pdf -pdflatex="pdflatex --shell-escape %O %S" $(SOURCE)

data:
	mkdir -p data || true

tikz:
	mkdir -p tikz || true

clean:
	rm -fr $(TARGET_TEX) tikz/ *.vrb *.dvi *.pdf *.aux *.auxlock *.fdb_latexmk *.fls *.log *.nav *.out *.snm *.toc || true

data/networksize.csv: data
	wget -O $@ https://metrics.torproject.org/networksize.csv?start=$(DATA_START_DATE)\&end=$(DATA_END_DATE)

data/platforms.csv: data
	wget -O $@ https://metrics.torproject.org/platforms.csv?start=$(DATA_START_DATE)\&end=$(DATA_END_DATE)

data/bandwidth-flags.csv: data
	wget -O $@ https://metrics.torproject.org/bandwidth-flags.csv?start=$(DATA_START_DATE)\&end=$(DATA_END_DATE)

data/versions.csv: data
	wget -O $@ https://metrics.torproject.org/versions.csv?start=$(DATA_START_DATE)\&end=$(DATA_END_DATE)

.PHONY: clean
